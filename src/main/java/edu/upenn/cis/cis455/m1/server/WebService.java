/**
 * CIS 455/555 route-based HTTP framework
 * 
 * V. Liu, Z. Ives
 * 
 * Portions excerpted from or inspired by Spark Framework, 
 * 
 *                 http://sparkjava.com,
 * 
 * with license notice included below.
 */

/*
 * Copyright 2011- Per Wendel
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;

// called inside of main(), which is inside of webserver.java
public class WebService {
    final static Logger logger = LogManager.getLogger(WebService.class);

    private HttpListener listener;
    private HttpTaskQueue taskqueue;
    private HttpWorker worker1;
    
    // fields 
    private String ipaddress = "0.0.0.0";
    private String rootDir;
    private int port;
    private int threadPoolSize;
    private int defaultQueueSize = 10;
    
    private Thread listenerThread;
    private Thread workerThread1;
    
    

    /**
     * Launches the Web server thread pool and the listener
     */
    // listener is the server daemon while loop, accepting server socket request.
    // listener needs to be run in a new thread
    public void start() {
    	logger.debug("webservice start() called, creating new HttpListener and HttpWorker\n");
    	// initialize the listener here
    	taskqueue = new HttpTaskQueue(defaultQueueSize);
    	listener = new HttpListener(taskqueue, taskqueue.getCap(), port);
    	worker1 = new HttpWorker(taskqueue, taskqueue.getCap(), rootDir);
        
        // start both listener and worker
    	workerThread1 = new Thread(worker1);
        workerThread1.start();
        
        listenerThread = new Thread(listener);
        listenerThread.start();
    }

    /**
     * Gracefully shut down the server
     * @throws IOException 
     */
    public void stop() throws IOException {
    	logger.debug("stop called\n");
    	listener.setInactive();
    	worker1.setInactive();
    	taskqueue.notifyAll();
    	
    }

    /**
     * Hold until the server is fully initialized.
     * Should be called after everything else.
     */
    public void awaitInitialization() {
        logger.info("Initializing server\n");
        start();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt() {
        throw new HaltException();
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode) {
        throw new HaltException(statusCode);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(String body) {
        throw new HaltException(body);
    }

    /**
     * Triggers a HaltException that terminates the request
     */
    public HaltException halt(int statusCode, String body) {
        throw new HaltException(statusCode, body);
    }

    ////////////////////////////////////////////
    // Server configuration
    ////////////////////////////////////////////

    /**
     * Set the root directory of the "static web" files
     */
    public void staticFileLocation(String directory) {
    	this.rootDir = directory;
    }

    /**
     * Set the IP address to listen on (default 0.0.0.0)
     */
    public void ipAddress(String ipAddress) {
    	this.ipaddress = ipAddress;
    }

    /**
     * Set the TCP port to listen on (default 45555)
     */
    public void port(int port) {
    	this.port = port; 
    }

    /**
     * Set the size of the thread pool
     */
    public void threadPool(int threads) {
    	this.threadPoolSize = threads;
    }

}
