package edu.upenn.cis.cis455.m1.handling;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import edu.upenn.cis.cis455.m1.server.HttpRequest;
import edu.upenn.cis.cis455.m1.server.HttpTask;

/**
 * Handles marshaling between HTTP Requests and Responses
 */
public class HttpIoHandler {
    final static Logger logger = LogManager.getLogger(HttpIoHandler.class);

    /**
     * Sends an exception back, in the form of an HTTP response code and message.
     * Returns true if we are supposed to keep the connection open (for persistent
     * connections).
     */
    public static boolean sendException(Socket socket, Request request, HaltException except) {
    	try {
    		PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
    		out.println("HTTP/1.1 " + except.statusCode() + " " + except.body());
    		logger.debug("HTTP/1.1 " + except.statusCode() + " " + except.body() + "\n");
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	return true;
    }

    /**
     * Sends data back. Returns true if we are supposed to keep the connection open
     * (for persistent connections).
     */
    // by the time we call this, the Response object should already have the contents 
    // of its message stored and retrievable with response.body()
    public static boolean sendResponse(Socket socket, Request request, Response response) {
    	try {
    		OutputStream out = socket.getOutputStream();
    		out.write(response.getHeaders().getBytes());
    		out.write("\r\n".getBytes());
			String bodyStr = response.body();
			if (bodyStr != "") {
				logger.debug("response has body\n");
	    		out.write(response.bodyRaw());
			} else {
				logger.debug("response body empty, status " + response.status() + "\n");
			}
			out.flush();
			out.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	return false; // assume no persistent connection for MS1
    }
    
    
    public static HttpRequest createRequest(HttpTask task) {
    	logger.debug("parsing the task...\r\n");
    	Socket s = task.getSocket();
    	HttpRequest r = new HttpRequest(s);
    	return r;
    }
    
    public static void fillDefaultHeaders(Request req, Response response) {
    	response.header("Content-Type", req.contentType());
    	
    	int contentLength = 0;
		if (response.bodyRaw() != null) {
			contentLength = response.bodyRaw().length;
		}
    	response.header("Content-Length", Integer.toString(contentLength));
    	
    	response.header("Connection", "close");
    }
    
    
    
    
    
}
