package edu.upenn.cis.cis455.m1.server;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub class for implementing the queue of HttpTasks
 */
public class HttpTaskQueue {
	
	static final Logger logger = LogManager.getLogger(HttpListener.class);
	
	// fields
	private int capacity;
	private ArrayList<HttpTask> q = new ArrayList<HttpTask>();
	
	// constructor
	public HttpTaskQueue(int cap) {
		capacity = cap;
	}
	
	// methods
	public int getSize() {
		return q.size();
	}
	
	public int getCap() {
		return capacity;
	}
	
	public void add(HttpTask task) {
		q.add(task);
		logger.info("added something to the taskqueue, taskqueue size is now " + getSize() + "\n");
	}
	
	public boolean isEmpty() {
		return q.isEmpty();
	}
	
	public HttpTask poll() {
		return q.remove(0);
	}
}
