package edu.upenn.cis.cis455.m1.server;

import java.net.Socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m1.handling.HttpRequestHandler;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.interfaces.Session;
import edu.upenn.cis.cis455.m2.server.RouteHandlers;

/**
 * Stub class for a thread worker that handles Web requests
 */
public class HttpWorker implements Runnable {
	static final Logger logger = LogManager.getLogger(HttpWorker.class);	
	
	private final HttpTaskQueue sharedQueue; // should this be final?
	private final int queueCap;
	private final String rootDir;
	private boolean active = true;

	// constructor
	public HttpWorker(HttpTaskQueue q, int cap, String rootdir) {
		sharedQueue = q;
		this.queueCap = cap;
		this.rootDir = rootdir;
	}
	
	/**
	 * Method to read from the queue.
	 * @return - element read from queue
	 * @throws InterruptedException
	 */
	private void readFromQueue() throws InterruptedException { // should not be void? , change later
		while (active) {
			synchronized (sharedQueue) {
				if (sharedQueue.isEmpty()) {
					//If the queue is empty, we push the current thread to waiting state. Way to avoid polling.
					logger.debug("Queue is currently empty\n");
					sharedQueue.wait();
				} else {
					HttpTask task = sharedQueue.poll();
					Socket s = task.getSocket();
					// need to parse the task, create a request object and give it to the iohandler
					HttpRequest httpreq = HttpIoHandler.createRequest(task);
					Response response = new HttpResponse();
					
					if (httpreq.uri() != "") { // if "" then an exception has been sent
						
						// set cookie with sessionid in response
						if (httpreq.session(false) != null) {
							// here we ignore the edge case where the session expires right after checking it
							response.cookie("JSESSIONID", httpreq.session(false).id());
						}
						
						
						Request req = (Request) httpreq; // cast
						
						// apply before filters
						RouteHandlers.matchBeforeAndExecute(s, req, response);
						
						// try to get route. if returns null then try static content
						Route route = httpreq.getRoute();
						if (route != null) {
							RouteHandlers.writeResponse(s, route, req, response);
						} else {
							HttpRequestHandler.handle(req, response, rootDir);
						}
						
						// apply after filters
						RouteHandlers.matchAfterAndExecute(s, req, response);
					
						// send the final response to socket
						HttpIoHandler.fillDefaultHeaders(req, response);
						HttpIoHandler.sendResponse(s, req, response);
					}
					
					
					logger.debug("Notifying everyone we have removed a task and sent the response\n");
					sharedQueue.notifyAll();
					logger.debug("Exiting queue with return\n");
					return;
				}
			}
		}
	}
	
	
    @Override
    public void run() {
    	while(active) {
			try {
				readFromQueue();
//				Thread.sleep(100); // keep or not?
			} catch (InterruptedException ex) {
				logger.error("Interrupt Exception in Consumer thread\n");
			}
		}
    }
    
    public void setInactive() {
    	active = false;
    }
}
