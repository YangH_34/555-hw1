package edu.upenn.cis.cis455.m1.server;


import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.cis455.m2.interfaces.Response;

public class HttpResponse extends Response {
	
	private Map<String, String> headerMap = new HashMap<String, String>();
	
	private Map<String, String> cookieMap = new HashMap<String, String>();
	
	public HttpResponse() {
	}

	@Override
	public String getHeaders() {
		String message = "";
		if (statusCode == 200) {
			message = "OK";
		} else if (statusCode == 404) {
			message = "Not Found";
		} else if (statusCode == 301) {
			message = "Moved Permanently";
		}
			
			
		
		String res = "HTTP/1.1 " + statusCode + " " + message + "\r\n";
		
//		String res = "HTTP/1.1 " + statusCode + " " + message + "\r\n" + 
//					 "Content-Type: " + contentType + "\r\n" + 
//					 "Content-Length: " + contentLength + "\r\n" + 
//					 "Connection: close\r\n";
		
		for (Map.Entry e: headerMap.entrySet()) {
			String key = (String) e.getKey();
			String value = (String) e.getValue();
			res += key + ": " + value + "\r\n";
		}
		
		
//		System.out.println("response header check: ");
//		System.out.println(res);
		return res;
	}

	
	// m2 methods
	@Override
	public void header(String header, String value) {
		headerMap.put(header, value);
		body();
	}

	@Override
	public void redirect(String location) {
		header("Location", location);
		contentType = "text/html";
		
		String s = "<html>\n"
				+ "<head>\n"
				+ "<title>Moved</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "<h1>Moved</h1>\n"
				+ "<p>This page has moved to <a href=\"" + location + "\">" + location + "</a>.</p>"
				+ "</body>\n"
				+ "</html>";
		
		body(s);
		
	}

	@Override
	public void redirect(String location, int httpStatusCode) {
		statusCode = httpStatusCode;
		header("Location", location);
		contentType = "text/html";
		
		String s = "<html>\n"
				+ "<head>\n"
				+ "<title>Moved</title>\n"
				+ "</head>\n"
				+ "<body>\n"
				+ "<h1>Moved</h1>\n"
				+ "<p>This page has moved to <a href=\"" + location + "\">" + location + "</a>.</p>"
				+ "</body>\n"
				+ "</html>";
		
		body(s);
	}

	@Override
	public void cookie(String name, String value) {
		// the least we have to give is a name and value
		String message = name + "=" + value + ";";
		cookieMap.put(name, message);
	}

	@Override
	public void cookie(String name, String value, int maxAge) {
		String message = name + "=" + value + "; Max-Age=" + maxAge + ";";
		cookieMap.put(name, message);
	}

	@Override
	public void cookie(String name, String value, int maxAge, boolean secured) {
		String sec = "";
		if (secured) {
			sec = "; Secure";
		}
		String message = name + "=" + value + "; Max-Age=" + maxAge + sec + ";";
		cookieMap.put(name, message);
		
	}

	@Override
	public void cookie(String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		String sec = "";
		if (secured) {
			sec = "; Secure";
		}
		String http = "";
		if (httpOnly) {
			http = "; HttpOnly";
		}
		String message = name + "=" + value + "; Max-Age=" + maxAge + sec + http + ";";
		cookieMap.put(name, message);
		
	}

	@Override
	public void cookie(String path, String name, String value) {
		String key = name + ";" + path + ";";
		String message = name + "=" + value + "; Path=" + path + ";";
		cookieMap.put(key, message);
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge) {
		String key = name + ";" + path + ";";
		String message = name + "=" + value + "; Path=" + path + "; Max-Age=" + maxAge + ";";
		cookieMap.put(key, message);
		
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured) {
		String sec = "";
		if (secured) {
			sec = "; Secure";
		}
		String key = name + ";" + path + ";";
		String message = name + "=" + value + "; Path=" + path + "; Max-Age=" + maxAge + sec + ";";
		cookieMap.put(key, message);
		
	}

	@Override
	public void cookie(String path, String name, String value, int maxAge, boolean secured, boolean httpOnly) {
		String sec = "";
		if (secured) {
			sec = "; Secure";
		}
		String http = "";
		if (httpOnly) {
			http = "; HttpOnly";
		}
		String key = name + ";" + path + ";";
		String message = name + "=" + value + "; Path=" + path + "; Max-Age=" + maxAge + sec + http + ";";
		cookieMap.put(key, message);
		
	}

	@Override
	public void removeCookie(String name) {
		if (cookieMap.containsKey(name)) {
			String message = cookieMap.get(name);
			String newMessage = "";
			String tmp = "";
			boolean hasMaxAge = false;
			for (int i = 0; i < message.length(); i++) {
				char c = message.charAt(i);
				tmp += c;
				
				if (tmp.equals("Max-Age=")) {
					newMessage += tmp;
					newMessage += "0";
					tmp = "";
				} else {
					if (c == ';') {
						newMessage += tmp;
						tmp = "";
					}
				}
			}
			if (!hasMaxAge) {
				newMessage += " Max-Age=0;";
			}
			cookieMap.replace(name, newMessage);
		}
	}

	@Override
	public void removeCookie(String path, String name) {
		String key = name + ";" + path + ";";
		if (cookieMap.containsKey(key)) {
			String message = cookieMap.get(key);
			String newMessage = "";
			String tmp = "";
			boolean hasMaxAge = false;
			for (int i = 0; i < message.length(); i++) {
				char c = message.charAt(i);
				tmp += c;
				
				if (tmp.equals("Max-Age=")) {
					newMessage += tmp;
					newMessage += "0";
					tmp = "";
				} else {
					if (c == ';') {
						newMessage += tmp;
						tmp = "";
					}
				}
			}
			if (!hasMaxAge) {
				newMessage += " Max-Age=0;";
			}
			cookieMap.replace(key, newMessage);
		}	
	}
}
