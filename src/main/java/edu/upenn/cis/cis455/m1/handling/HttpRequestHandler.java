package edu.upenn.cis.cis455.m1.handling;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.m1.interfaces.Request;
import edu.upenn.cis.cis455.m1.interfaces.Response;
import edu.upenn.cis.cis455.m1.server.HttpResponse;

public class HttpRequestHandler {
	final static Logger logger = LogManager.getLogger(HttpIoHandler.class);
	
	// my handle method
    public static void handle(Request request, Response response, String rootDir) {
    	try {
        	String method = request.requestMethod();
        	String pathInfo = request.pathInfo();
        	Path path = Paths.get(rootDir + pathInfo);
        	logger.debug("full path with rootdir is " + path + "\n");
        	
        	// special cases: /control and /shutdown -- should only work if method is GET?
        	if (pathInfo.equals("/shutdown")) {
        		response.status(200);
        		logger.debug("/shutdown is recognized\n");
        		SparkController.stop();
        		return;
        	}
        	if (pathInfo.equals("/control")) {
        		response.status(200);
        		String panel = "<!DOCTYPE html>\n"
        					+ "<html>\n"
	        				+ "<head>\n"
	        				+ "    <title>Control Panel</title>\n"
	        				+ "</head>\n"
	        				+ "<body>\n"
	        				+ "<h1>Thread Information</h1>\n"
	        				+ "<ul>\n"
	        				+ "<li><a href=\"/shutdown\">Shut down</a></li>\n"
	        				+ "</ul>\n"
	        				+ "</body>\n"
	        				+ "</html>";	
        		
        		
        		response.body(panel);
        		response.type("text/html");
        		return;
        	}
        	
        	
        	byte[] b = Files.readAllBytes(path);
        	
        	// GET and HEAD
        	if (method.equals("GET") || method.equals("HEAD")) {
        		response.status(200);
   
        		// decide on MIME type
        		if (request.uri().endsWith(".jpg")) {
        			response.type("image/jpg");
        		} else if (request.uri().endsWith(".gif")) {
        			response.type("image/gif");
        		} else if (request.uri().endsWith(".png")) {
        			response.type("image/png");
        		} else if (request.uri().endsWith(".txt")) {
        			response.type("text/plain");
        		} else if (request.uri().endsWith(".html") || request.uri().endsWith(".htm")) {
        			response.type("text/html");
        		}
        		
        		if (method.equals("GET")) {
        			response.bodyRaw(b);
        		}
        		
        	} else {
        		// only have get and head for MS1
        		response.status(404);
        		System.out.println("unsupported operation yet");
        	}
        	
    	} catch (IOException e) {
    		response.status(404);
    		logger.debug("File does not exist\n");
    	}
    	
    }
}
