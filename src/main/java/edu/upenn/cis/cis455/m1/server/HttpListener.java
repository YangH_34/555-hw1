package edu.upenn.cis.cis455.m1.server;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Stub for your HTTP server, which listens on a ServerSocket and handles
 * requests
 */
public class HttpListener implements Runnable {
	static final Logger logger = LogManager.getLogger(HttpListener.class);

	// fields
	private final HttpTaskQueue sharedQueue;
	private final int queueCap;
	private int portNumber;
	private boolean active = true;
	private ServerSocket ssPtr;
	
	// constructor
	public HttpListener(HttpTaskQueue q, int cap, int port) {
		sharedQueue = q;
		queueCap = cap;
		portNumber = port;
	}
	
	private void addToQueue(HttpTask task) throws InterruptedException {
		while (active) {
			synchronized (sharedQueue) {
				if (sharedQueue.getSize() == queueCap) {
					logger.debug("sharedQueue is full!\n");
					sharedQueue.wait();
				} else {
					sharedQueue.add(task);
					sharedQueue.notifyAll();
					break;
				}
			}
		}
	}
	
    @Override
    public void run() {
    	logger.debug("listener running...\n");
    	try (ServerSocket ss = new ServerSocket(portNumber)) {
    		ssPtr = ss;
    		while (active) {
        		// receive requests and push them to sharedQueue
    			Socket acceptedSocket = ss.accept();
    			HttpTask task = new HttpTask(acceptedSocket);
        		addToQueue(task);
        		logger.debug("end of loop for listener\n");
        	}
    	} catch (IOException | InterruptedException e) {
    		e.printStackTrace();
    	}
    }
    
    public void setInactive() throws IOException {
    	ssPtr.close();
    	active = false;
    	
    }
}
