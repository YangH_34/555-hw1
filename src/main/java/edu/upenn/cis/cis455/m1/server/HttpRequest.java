package edu.upenn.cis.cis455.m1.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.interfaces.Session;
import edu.upenn.cis.cis455.m2.server.HttpSession;
import edu.upenn.cis.cis455.m2.server.RouteHandlers;
import edu.upenn.cis.cis455.m2.server.SessionWarehouse;

public class HttpRequest extends Request {
	
	final static Logger logger = LogManager.getLogger(HttpIoHandler.class);
	
	private String requestMethod;
	private String host;
	private String userAgent;
	private int port; 
	private String pathInfo;
//	private String url;
	private String uri = ""; 
	private String protocol;
	private Route route = null;
//	private String contentType;
	
	private HashMap<String, String> headers;
	private HashMap<String, List<String>> parms;
	
	private Map<String, String> routeParameters = new HashMap<String, String>();
	private Map<String, Object> attributeMap = new HashMap<String, Object>();
	private Map<String, String> cookieMap = new HashMap<String, String>();
	
	private Session currSession;
	
	
	
	public HttpRequest(Socket s) {
		host = s.getLocalAddress().toString();
		port = s.getLocalPort();
		
		// parsing
		try {
			// parse and populate headers
			InputStream	input = s.getInputStream();
			String remoteIp = s.getRemoteSocketAddress().toString();
			this.headers = new HashMap<String, String> ();
			this.parms = new HashMap<String, List<String>>();
			this.uri = HttpParser.parseRequest(remoteIp, input, headers, parms, s, this);
			
			logger.debug("parsed uri is " + uri + "\n"); // I am not sure if this is the real uri or not
			logger.debug("queryString is " + headers.get("queryString") + "\n");
			logger.debug("method is " + requestMethod + "\n");
			
			this.requestMethod = headers.get("Method");
			this.pathInfo = headers.get("pathInfo");
			this.protocol = headers.get("protocolVersion");
			
			
			
			// parse the cookies
			if (headers.get("cookie") != null) {
				parseCookies(headers.get("cookie"));
				// need to check in cookies for JSESSIONID an associated it with a current session
				if (cookieMap.containsKey("JSESSIONID")) {
					String sessionID = cookieMap.get("JSESSIONID");
					// if there is a valid session with the id, associate it with this request
					// if invalid then currSession will be null
					currSession = SessionWarehouse.checkAndGetSession(sessionID);
				}
			}
			
			// route handling -- populate params and qeurymap
			if (uri != "") { // currently treating empty path as bad request
				route = RouteHandlers.matchRoute(requestMethod, pathInfo, routeParameters);
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
//		// ------------------- print checks ---------------------
//		// checking parms -- are the queryparams stored in here?
//		System.out.println("key-value pairs in parms:");
//		for (Map.Entry tmp: parms.entrySet()) {
//			String key = (String) tmp.getKey();
//			System.out.println("key is: " + key);
//			List<String> valueList = (List<String>) tmp.getValue();
//			for (String str: valueList) {
//				System.out.print(str + ", ");
//			}
//			System.out.println();
//		}
//		System.out.println("\n");
//		
//		
//		// checking headers
//		System.out.println("key-value pairs in headers:");
//		for (Map.Entry tmp: headers.entrySet()) {
//			String key = (String) tmp.getKey();
//			String value = (String) tmp.getValue();
//					
//			System.out.println(key + ": " + value);
//		}
//		System.out.println("\n");
//		
//		
//		// checking cookies
//		System.out.println("key-value pairs in cookies:");
//		for (Map.Entry tmp: cookieMap.entrySet()) {
//			String key = (String) tmp.getKey();
//			String value = (String) tmp.getValue();
//					
//			System.out.println(key + ": " + value);
//		}
//		System.out.println("\n");
//		
//		
//		// checking route parameters
//		System.out.println("key-value pairs in route params:");
//		for (Map.Entry tmp: routeParameters.entrySet()) {
//			String key = (String) tmp.getKey();
//			String value = (String) tmp.getValue();
//					
//			System.out.println(key + ": " + value);
//		}
//		System.out.println("\n");
//		// -------------- end of print checks -----------------
		
	}
	
	
	private void parseCookies(String cookieString) {
		String name = "";
		String val = "";
		String tmp = "";
		for (int i = 0; i < cookieString.length(); i++) {
			char c = cookieString.charAt(i);
			if (c == '=') {
				name = tmp;
				tmp = "";
			} else if (c == ';') {
				val = tmp;
				tmp = "";
				cookieMap.put(name, val);
			} else if (c == ' ') {
				continue;
			} else {
				tmp += c;
				if (i == cookieString.length() - 1) {
					val = tmp;
					cookieMap.put(name, val);
				}
			}
		}
	}
	
	
	public Route getRoute() {
		return route;
	}
	
	@Override
	public String requestMethod() {
		return requestMethod;
	}

	@Override
	public String host() {
		return host;
	}

	@Override
	public String userAgent() {
		return null;
	}

	@Override
	public int port() {
		return port;
	}

	@Override
	public String pathInfo() {
		return pathInfo;
	}

	@Override
	public String url() {
		return null;
	}

	@Override
	public String uri() {
		return uri;
	}

	@Override
	public String protocol() {
		return protocol;
	}

	@Override
	public String contentType() {
		return null;
	}

	@Override
	public String ip() {
		return null;
	}

	@Override
	public String body() {
		return null;
	}

	@Override
	public int contentLength() {
		return 0;
	}

	@Override
	public String headers(String name) {
		return headers.get(name);
	}

	@Override
	public Set<String> headers() {
		return headers.keySet();
	}

	
	// m2 methods
	@Override
	public Session session() {
		if (currSession == null || (!SessionWarehouse.hasSession(currSession.id()))) {
			currSession = new HttpSession();
			SessionWarehouse.put(currSession.id(), currSession);
		}
		return currSession;
	}

	@Override
	public Session session(boolean create) {
		if (currSession == null || (!SessionWarehouse.hasSession(currSession.id()))) {
			// if there is no valid session -- result depends on "create" boolean
			if (create) {
				currSession = new HttpSession();
				SessionWarehouse.put(currSession.id(), currSession);
				return currSession;
			} else {
				return null;
			}
		} else {
			// if there is a valid session then just return it
			return currSession;
		}
	}
	
	

	@Override
	public Map<String, String> params() {
		return routeParameters;
	}

	@Override
	public String queryParams(String param) {
		if (parms.containsKey(param)) {
			return parms.get(param).get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<String> queryParamsValues(String param) {
		return parms.get(param);
	}

	@Override
	public Set<String> queryParams() {
		return parms.keySet();
	}

	@Override
	public String queryString() {
		return headers.get("queryString");
	}

	@Override
	public void attribute(String attrib, Object val) {
		attributeMap.put(attrib, val);
	}

	@Override
	public Object attribute(String attrib) {
		return attributeMap.get(attrib);
	}

	@Override
	public Set<String> attributes() {
		return attributeMap.keySet();
	}

	@Override
	public Map<String, String> cookies() {
		return cookieMap;
	}

}
