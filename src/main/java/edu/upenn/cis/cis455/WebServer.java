package edu.upenn.cis.cis455;

import static edu.upenn.cis.cis455.SparkController.*;

import org.apache.logging.log4j.Level;

import edu.upenn.cis.cis455.m1.server.WebService;

/**
 * Initialization / skeleton class.
 * Note that this should set up a basic web server for Milestone 1.
 * For Milestone 2 you can use this to set up a basic server.
 * 
 * CAUTION - ASSUME WE WILL REPLACE THIS WHEN WE TEST MILESTONE 2,
 * SO ALL OF YOUR METHODS SHOULD USE THE STANDARD INTERFACES.
 * 
 * @author zives
 *
 */
public class WebServer {
    public static void main(String[] args) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);

        // TODO: make sure you parse *BOTH* command line arguments properly --done
        // All user routes should go below here...
        int port = 4555;
        String root = "./www";
        if (args.length >= 2) {
        	System.out.println("first arg is " + args[0]);
            System.out.println("second arg is " + args[1]);
            port = Integer.parseInt(args[0]);
            root = args[1];
        }
        
        SparkController.port(port);
        SparkController.staticFileLocation(root);
        
        get("/testRoute", (request, response) -> {
            return "testRoute content";
        });
        
        get("/:name/hello", (request, response) -> {
            return "Hello: " + request.params("name");
        });
        
        get("/testCookie1", (request, response) -> {
            String body = "<HTML><BODY><h3>Cookie Test 1</h3>";
            
            response.cookie("TestCookie1", "1");

            body += "Added cookie (TestCookie,1) to response.";
            response.type("text/html");
            response.body(body);
            return response.body();
          });
        
        
        
        get("/testSession1", (request, response) -> {
            String body = "<HTML><BODY><h3>Session Test 1</h3>";

            request.session(true).attribute("Attribute1", "Value1");

            body += "</BODY></HTML>";
            response.type("text/html");
            response.body(body);
            return response.body();
          });
        
        
          before((request, response) -> {
            request.attribute("attribute1", "everyone");
          });

          get("/testFilter1", (request, response) -> {
            String body = "<HTML><BODY><h3>Filters Test</h3>";

            for(String attribute : request.attributes()) {
              body += "Attribute: " + attribute + " = " + request.attribute(attribute) + "\n";
            }

            body += "</BODY></HTML>";
            response.type("text/html");
            response.body(body);
            return response.body();
          });

          after((req, res) ->{});
        
        
        
        // ... and above here. Leave this comment for the Spark comparator tool

        System.out.println("Waiting to handle requests!");
        awaitInitialization();
    }
}
