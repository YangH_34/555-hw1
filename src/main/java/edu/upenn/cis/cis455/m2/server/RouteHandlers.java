package edu.upenn.cis.cis455.m2.server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.SparkController;
import edu.upenn.cis.cis455.exceptions.HaltException;
import edu.upenn.cis.cis455.m1.handling.HttpIoHandler;
import edu.upenn.cis.cis455.m2.interfaces.Filter;
import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Response;
import edu.upenn.cis.cis455.m2.interfaces.Route;

public class RouteHandlers {
	
	static final Logger logger = LogManager.getLogger(RouteHandlers.class);
	
	// We don't want people to use the constructor
    protected RouteHandlers() {}
	
	private static List<SimpleEntry<String, SimpleEntry<String, Route>>> list
			= new ArrayList<SimpleEntry<String, SimpleEntry<String, Route>>>();
	
	private static List<SimpleEntry<String, Filter>> beforeFilterList
	= new ArrayList<SimpleEntry<String, Filter>>();
	
	private static List<SimpleEntry<String, Filter>> afterFilterList
	= new ArrayList<SimpleEntry<String, Filter>>();
	
	
	
	
	// --------------- Filter Functions ------------------------
	public static void addBefore(Filter filter) {
		beforeFilterList.add(new SimpleEntry("///", filter));
	}
	
	public static void addBefore(String path, Filter filter) {
		beforeFilterList.add(new SimpleEntry(path, filter));
	}
	
	public static void addAfter(Filter filter) {
		afterFilterList.add(new SimpleEntry("///", filter));
	}
	
	public static void addAfter(String path, Filter filter) {
		afterFilterList.add(new SimpleEntry(path, filter));
	}
	
	public static void matchBeforeAndExecute(Socket s, Request request, Response response) {
		try {
			String path = request.pathInfo();
			for (SimpleEntry<String, Filter> entry: beforeFilterList) {
				String currPath = entry.getKey();
				Filter currFilter = entry.getValue();
				if (currPath.equals("///") || matchPath(path, currPath, request.params())) {
					currFilter.handle(request, response);
				}
			}
		} catch (HaltException h) {
			HttpIoHandler.sendException(s, request, h);
		} catch (Exception e) {
			response.status(404);
		}
	}
	
	
	public static void matchAfterAndExecute(Socket s, Request request, Response response) {
		try {
			String path = request.pathInfo();
			for (SimpleEntry<String, Filter> entry: afterFilterList) {
				String currPath = entry.getKey();
				Filter currFilter = entry.getValue();
				if (currPath.equals("///") || matchPath(path, currPath, request.params())) {
					currFilter.handle(request, response);
				}
			}
		} catch (HaltException h) {
			HttpIoHandler.sendException(s, request, h);
		} catch (Exception e) {
			response.status(404);
		}
	}

	
	
	// ------------------ Route Functions--------------------------
	
	public static void addRoute(String method, String path, Route route) {
		//use a list. need to preserve order of registration.
		list.add(new SimpleEntry(method, new SimpleEntry(path, route)));
	}
	
	public static Route matchRoute(String method, String path, Map<String, String> routeParameters) {
		for (SimpleEntry<String, SimpleEntry<String, Route>> entry: list) {
			String currMethod = entry.getKey();
			String currPath = entry.getValue().getKey();
			Route currRoute = entry.getValue().getValue();
			if (method.equals(currMethod) && matchPath(path, currPath, routeParameters)) {
				return currRoute;
			}
		}
		return null;
	}
	
	private static boolean matchPath(String requestPath, String currPath, Map<String, String> params) {
		List<String> reqList = new ArrayList<String>();
		String tmp = "";
		for (int i = 0; i < requestPath.length(); i++) {
			char c = requestPath.charAt(i);
			if (c == '/') {
				reqList.add(tmp);
				tmp = "";
			} else {
				tmp += c;
				if (i == requestPath.length() - 1) {
					reqList.add(tmp);
				}
			}
		}
		tmp = "";
		
		List<String> currList = new ArrayList<String>();
		for (int i = 0; i < currPath.length(); i++) {
			char c = currPath.charAt(i);
			if (c == '/') {
				currList.add(tmp);
				tmp = "";
			} else {
				tmp += c;
				if (i == currPath.length() - 1) {
					currList.add(tmp);
				}
			}
		}
		
		// checking if match
		if (reqList.size() != currList.size()) {
			return false;
		}
		for (int i = 0; i < reqList.size(); i++) {
			String reqStr = reqList.get(i);
			String currStr = currList.get(i);
			if ((!reqStr.equals(currStr)) && !(currStr.startsWith(":") || currStr == "*")) {
					return false;
			}
		}
		
		// populate parameters
		for (int i = 0; i < reqList.size(); i++) {
			String reqStr = reqList.get(i);
			String currStr = currList.get(i);
			if (currStr.startsWith(":")) {
				params.put(currStr, reqStr);
			}
		}
		
		return true;
	}
	
	
	
	public static void writeResponse(Socket s, Route route, Request request, Response response) {
		try {
			response.status(200);
			response.type("text/plain");
			Object o = route.handle(request, response);
			if (response.body() == "" && o != null) {
				response.body(o.toString());
			}
		} catch (HaltException h) {
			HttpIoHandler.sendException(s, request, h);
		} catch (Exception e) {
			response.status(404);
		}
		
	}
	
}
