package edu.upenn.cis.cis455.m2.server;

import java.sql.Time;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import edu.upenn.cis.cis455.m2.interfaces.Request;
import edu.upenn.cis.cis455.m2.interfaces.Route;
import edu.upenn.cis.cis455.m2.interfaces.Session;

public class HttpSession extends Session{
	
	private Request request; // should have a request? -- not sure
	private final long creationTimeField;
	private final String uniqueID;
	private int timeoutInterval = 1800; // in seconds, default value 1800 sec = 30 min
	private long lastAccessedTimeField;
	
	private boolean invalid = false;
	
	private Map<String, Object> attributeMap = new HashMap<String, Object>();
	
	
	public HttpSession() {
		creationTimeField = System.currentTimeMillis();
		uniqueID = UUID.randomUUID().toString(); // this id gets sent back to the browser in the form of a cookie?
		lastAccessedTimeField = creationTimeField;
	}
	

	@Override
	public String id() {
		access();
		return uniqueID;
	}

	@Override
	public long creationTime() {
		access();
		return creationTimeField;
	}

	@Override
	public long lastAccessedTime() { // in milliseconds
		access();
		return lastAccessedTimeField;
	}

	@Override
	public void invalidate() {
		invalid = true;
		attributeMap.clear();
		// delete itself from the sessionWarehouse
		SessionWarehouse.remove(uniqueID);
	}

	
	@Override
	public int maxInactiveInterval() { // in seconds
		access();
		return timeoutInterval;
	}

	@Override
	public void maxInactiveInterval(int interval) { // in seconds
		access();
		timeoutInterval = interval;
		
	}

	@Override
	public void access() {
		if (invalid) {
			return;
		}
		
		long currTime = System.currentTimeMillis();
		if (currTime - lastAccessedTimeField > timeoutInterval * 1000) {
			invalidate();
		}
		lastAccessedTimeField = currTime;	
		
	}

	@Override
	public void attribute(String name, Object value) {
		if (invalid) {
			return;
		}
		access();
		attributeMap.put(name, value);
	}

	@Override
	public Object attribute(String name) {
		if (invalid) {
			return null;
		}
		access();
		return attributeMap.get(name);
	}

	@Override
	public Set<String> attributes() {
		if (invalid) {
			return null;
		}
		access();
		return attributeMap.keySet();
	}

	@Override
	public void removeAttribute(String name) {
		if (invalid) {
			return;
		}
		access();
		attributeMap.remove(name);
	}
	
}
