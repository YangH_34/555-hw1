package edu.upenn.cis.cis455.m2.server;

import java.util.HashMap;
import java.util.Map;

import edu.upenn.cis.cis455.m2.interfaces.Session;

public class SessionWarehouse {
	private static Map<String, Session> sessionMap = new HashMap<String, Session>(); 
	
	protected SessionWarehouse() {}
	
	public static boolean hasSession(String id) {
		if (sessionMap.containsKey(id)) {
			Session s = sessionMap.get(id);
			s.access(); // if expires then would delete itself from the sessionMap
			if (sessionMap.containsKey(id)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	public static void remove(String id) {
		sessionMap.remove(id);
	}
	
	
	public static Session checkAndGetSession(String id) {
		if (hasSession(id)) {
			return sessionMap.get(id);
		} else {
			return null;
		}
	}
	
	public static void put(String id, Session s) {
		sessionMap.put(id, s);
	}
}
